
import java.util.Scanner;

public class StudentGradingSystem {
	
	public static String getSubject(int sbj) {
		switch (sbj) {
		case 0:
			return "Maths";
		case 1:
			return "Science";
		case 2:
			return "English";
		case 3:
			return "History";
		default:
			return "Not found";
		}
	}

	// helps to grade the students
	public static String getRank(Double marks, int x) {
		marks = marks / x;
		if (marks <= 100 && marks >= 75) {
			return "A";
		} else if (marks <= 74 && marks >= 65) {
			return "B";
		} else if (marks <= 64 && marks >= 55) {
			return "C";
		} else if (marks <= 54 && marks >= 35) {
			return "D";
		} else if (marks <= 34) {
			return "F";
		} else {
			return "N/A";
		}
	}

	public static void main(String args[]) {

		String username, password;
		Scanner s = new Scanner(System.in);
		System.out.println("--------\" WELCOME TO STUDENT GRADING SYSTEM \"-------------");
		//System.out.println("\tLOGIN");
		System.out.println("Please enter the Username: ");
		username = s.nextLine();

		System.out.println("Please enter the Password: ");
		password = s.nextLine();

		if (username.equals("Prasha") && password.equals("Sha")
				|| username.equals("Prestina") && password.equals("Presti")
				|| username.equals("Kirushajiny") && password.equals("Kiru")) {
			System.out.println("Your login is Successful");
		} else {
			System.out.println("Your login is Failed");
		}

	}
}
